// 
// This is the script that runs when the spreadsheet associated to the form gets a new row,
// when someone submits the request form for the RCD CM. 
// The script is associated to the spreadsheet in drive and is archived here for safety and documentation. 
// In the Script editor for the spreadsheet, the Triggers must be configured so that 
// onFormSubmit invokes handleNewSubmission(). Everything else is configured in the script,
// including the logging sheet, etc. 
// This script requires the BetterLog library: https://github.com/peterherrmann/BetterLog
//
// Copyright (c) 2020, 2021 CaRCC. 
// Licensed under the Educational Community License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. You may obtain a copy of the License at
// http://www.osedu.org/licenses/ECL-2.0
// Unless required by applicable law or agreed to in writing, software distributed under the 
// License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, 
// either express or implied. See the License for the specific language governing permissions 
// and limitations under the License.
//
function handleNewSubmission(e) {
  Logger = BetterLog.useSpreadsheet('1uX_Fa9lkJNYPGu-ooz8EMKKeDXKqr5vAe4Ore7YjrHg');

  var row = e.range.getRow();
  var sourceSheet = e.range.getSheet();
  var a1Notation = 'B'+row;
  var newEmailCell = sourceSheet.getRange(a1Notation);
  var newEmail = newEmailCell.getValue();
  a1Notation = 'R'+row;
  var altEmailCell = sourceSheet.getRange(a1Notation);
  var altEmail = altEmailCell.getValue();
  a1Notation = 'C'+row;
  var univCell = sourceSheet.getRange(a1Notation);
  var university = univCell.getValue();
  // Handle case of not in list ("Other")
  if(university.startsWith("Other")) {
    a1Notation = 'N'+row;
    univCell = sourceSheet.getRange(a1Notation);
    university = univCell.getValue();
  }

  // a1Notation = 'D'+row;
  // var scopeCell = sourceSheet.getRange(a1Notation);
  // var scope = scopeCell.getValue();
  a1Notation = 'E'+row;
  var scopeDetCell = sourceSheet.getRange(a1Notation);
  var scopeDetail = scopeDetCell.getValue();
  var institutionName = university.trim();
  var regExp = new RegExp(scopeDetail.trim(), "i");
  if(university.search(regExp) < 0) {  // If the university name already has the location, do not append
    institutionName += ', ' + scopeDetail.trim();
  }
  
  try {
    
    var formURLCol = 99;         // Default the URL column to way out right
    var contactHistoryCol = 100; // Default the contact History column next to that
    try {
      // get the first row with the column headings  
      var headersRange = sourceSheet.getRange(1, 1, 1, 50);
      // Find the column where we store the URL of their copy of the Assessment tool
      var textFinder = headersRange.createTextFinder('Form Copy URL');
      if( textFinder != null) {
        var textFoundRange = textFinder.findNext();
        if(textFoundRange != null) {
          formURLCol = textFoundRange.getColumn();
        }
      }
      textFinder = headersRange.createTextFinder('Contact History');
      if( textFinder != null) {
        textFoundRange = textFinder.findNext();
        if(textFoundRange != null) {
          contactHistoryCol = textFoundRange.getColumn();
        }
      }
    } catch (e) {
      Logger.log('Caught exception: Trying to find URL/Contact History columns ' + e.name + ' msg: ' + e.message);
    }    
    // Logger.log('Found the url column: '+ formURLCol);
    
    // Check for a resubmission
    var newSheetURLCell = sourceSheet.getRange(row,formURLCol);
    if(newSheetURLCell.getValue() !== '') {
      Logger.log(institutionName + ' just updated their response to the form! No email being sent to: ' + newEmail);
      // If there is any logic to handle the perms, etc., add that here
      return;
    }

    Logger.log(institutionName + ' just responded to the form! Creating copy; will send msg to: ' + newEmail);
  
    var templateSS = SpreadsheetApp.openById("1LTNRzM2oSRQzjgTtT6B7SAqlI34Asm0cVe6OGMCH7GA"); // 1.1 version
    
    // Copy the template named for the new Institution. Note that it ends up in the script owner's root
    var newSheet = templateSS.copy(institutionName + " RCD Capabilities Model Assessment");
    var newSheetID = newSheet.getId();
    var newSheetURL = newSheet.getUrl();

    // Set the institution name into the summary page
    var instNameCell = newSheet.getRange("Summary!C2");
    instNameCell.setValue(institutionName);

    Logger.log('Created a new copy of v1.1 tool called: '+ newSheet.getName() + ' w/ ID: ' + newSheetID + ' available at: ' + newSheetURL);

    newSheetURLCell.setValue(newSheetURL);
    
    // Move the new sheet to the instition copies folder: /CaRCC Working Groups/CaRCC Capabilities Model/Assessment Tool/Institution Copies
    var newSheetFile = DriveApp.getFileById(newSheetID);

    var dest_folder = DriveApp.getFolderById('1sOVYfW5cRthnhHJrvypeJ9KXoCB5yzW1'); 
    dest_folder.addFile(newSheetFile);

    // remove it from my drive folder (the default destination)
    var source_folder = DriveApp.getRootFolder();
    source_folder.removeFile(newSheetFile);
    
    if(restrictPerms(newSheetFile, newSheet, newEmail)) {
      Logger.log('Set up perms for new sheet; added editor: ' + newEmail);
      
      sendNewMsg(newEmail, altEmail, institutionName, newSheetURL );
      var newSheetContactHistoryCell = sourceSheet.getRange(row,contactHistoryCol);
      var dateNow = new Date();
      newSheetContactHistoryCell.setValue('Emailed Copy Info on: '+ dateNow.toISOString());

      Logger.log('Sent mail to ' + newEmail);
    } else { // some problem with the email address and permissions. Email them a special note
      Logger.log('Error setting up perms for new sheet, added editor: ' + newEmail);
      
      sendProblemMsg(newEmail, altEmail, institutionName, newSheetURL );
      var newSheetContactHistoryCell = sourceSheet.getRange(row,contactHistoryCol);
      var dateNow = new Date();
      newSheetContactHistoryCell.setValue('Emailed Problem Note on: '+ dateNow.toISOString());

      Logger.log('Sent mail to ' + newEmail);
    }
    
  } catch (e) {
    Logger.log('Caught exception in handleNewSubmission: ' + e.name + ' msg: ' + e.message);
  }
}

function restrictPerms(newSheetFile, newSheet, newEmail) {
  try {
    // Remove all the editors for privacy
    var editors = newSheetFile.getEditors();
    if(editors != null) {
      for(var i=0;i<editors.length;i++) {
        newSheetFile.removeEditor(editors[i]);
      }
    }

    // Remove all the viewers for privacy
    var viewers = newSheetFile.getViewers();
    if(viewers != null) {
      for(var i=0;i<viewers.length;i++) {
        newSheetFile.removeViewer(viewers[i]);
      }
    }
    // Set file access to be Private
    if(newSheetFile.getSharingAccess() != DriveApp.Access.PRIVATE) {
      newSheetFile.setSharing(DriveApp.Access.PRIVATE, DriveApp.Permission.NONE);
    }
    // Ensure they will also be able to add other editors
    newSheetFile.setShareableByEditors(true);
    // Add requesting user to editors, so they can actually make changes.
    // Use the Spreadsheet object, not the File, to avoid the email invite
    newSheet.addEditor(newEmail);
    return true;
  } catch (e) {
    Logger.log('Caught exception in restrictPerms: ' + e.name + ' msg: ' + e.message);
    return false;
  }
}

function sendProblemMsg(newEmail, altEmail, newName, newSheetURL ) {
  sendMsg(newEmail, altEmail, newName, newSheetURL, true );
}

function sendNewMsg(newEmail, altEmail, newName, newSheetURL ) {
  sendMsg(newEmail, altEmail, newName, newSheetURL, false );
}

function sendMsg(newEmail, altEmail, newName, newSheetURL, problemEmail ) {
  var RCDCMWorkingGroupEmail = 'capsmodel-help@carcc.org';
  var RCDCMOwnerEmail = 'capsmodel-owner@carcc.org';
  var salutation = 'Dear ' + newName + ',';
  var mainMessage1 = 'Thank you for your interest in the RCD Capabilities Model Assessment tool. ' +
    'A copy of the V1.1 assessment tool specifically for your institution has been created, and is now available for your use.';
  var accessMessage, htmlAccessMessage;
  if(problemEmail) {
    accessMessage = 'Unfortunately, Google sheets could not add your email ('+newEmail+') as an editor. '+
    'Please contact us at '+RCDCMWorkingGroupEmail+' and we will work with you to establish access (e.g., if you have a gmail address, that will work).';
    htmlAccessMessage = accessMessage;
  } else {
    accessMessage = 'You can access your copy at: ' + newSheetURL +
      ' (you must be logged in to Google docs with this email address to access this).';
    htmlAccessMessage = 'You can access your copy by clicking <a href="'+newSheetURL+
      '">here</a> (you must be logged in to Google docs with this email address to access this).<br/>' +
      'You can also copy and paste the URL into your browser: <br/>' + newSheetURL;
  }
  var RCDCMIntroGuideTitle = 'Capabilities Model Introduction and Guide to Use';
  var RCDCMIntroGuideURL = 'https://docs.google.com/document/d/15xiDXMta7AlEvE6IpW4mvadAiW2PPshmBi73AVHTm9g/view';
  var SubmissionFormURL = 'https://docs.google.com/forms/d/e/1FAIpQLSdDjUYfcnoVVWbx3KjNFZi_1hVeqhTrQ-iv9FHk57eyAoljGw/viewform';
  var simpleMessage2 = 'We also provide a '+RCDCMIntroGuideTitle +', and we recommend you review it before working with your copy. '+
    'This document is available here: ' + RCDCMIntroGuideURL;
  var htmlMessage2 = 'We also provide a <a href="'+RCDCMIntroGuideURL+'">'+RCDCMIntroGuideTitle+
    '</a>, and we recommend you review it before working with your copy. ' +
      'The URL for this document is: <br/>' + RCDCMIntroGuideURL;
  var simpleMessage3 = 'When you’re done with your assessment, use the 2021 Community Dataset Submission Form to submit, available here: '+
     + SubmissionFormURL;
  var htmlMessage3 = 'When you’re done with your assessment, use the <a href="'+SubmissionFormURL+'">2021 Community Dataset Submission Form</a> to submit. ' +
      'The URL for this form is: <br/>' + SubmissionFormURL;
  var closingMessage = 'We hope you find the RCD Capabilities Model Assessment tool useful. ' +
    'Please let us know how you use it, and contact us if you have any questions. You can email the working group at: ';

  var simpleTextBody = salutation + '\n\n' + mainMessage1 + '\n\n' + accessMessage + '\n\n' +
    simpleMessage2 + '\n\n' + simpleMessage3 + '\n\n' +
    closingMessage + RCDCMWorkingGroupEmail + '\n\n';
  
  var htmlTextBody = '<html><body><p>' + salutation + '</p><p>' + mainMessage1 + '</p><p>' + htmlAccessMessage +
    '</p><p>'+htmlMessage2 + '</p><p>'+htmlMessage3 +
      '</p><p>'+closingMessage+ '<a href="mailto:'+RCDCMWorkingGroupEmail+'?subject=Re: RCD Capabilities Model Assessment tool">'+
        RCDCMWorkingGroupEmail+'</a>' + 
          '</p></body></html>';
  
  MailApp.sendEmail(newEmail,
                    "Your Institution's copy of the RCD Capabilities Model Assessment Tool",
                    simpleTextBody,
                  {
                    name: 'CaRCC RCD Capabilities Model request',
                    replyTo: RCDCMWorkingGroupEmail,
                    cc: altEmail,
                    bcc: RCDCMWorkingGroupEmail,
                    htmlBody: htmlTextBody
                  });
  
}