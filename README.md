# README #

This is the repo for the RCD Capabitlities Model, mostly for the assessment tool. 

## What is this repository for? ##

Initially, we are saving essential scripts, etc. for the GSheets implementations (v1.0 and v1.1).
For the 2.0 implementation we will add the scripts that are used in the survey tool, etc. Note that we will create separate repos for the major
elements of the RCD RCC (various portals, etc.). 

## Deployment ##

Note that the code for the v1.* implementations does not build or deploy, and must be copied into the 
script editor window for the spreadsheet that tracks requests for access to the RCDCM assessment tool. 

## Contribution guidelines ###

* Contributors must have all contributions reviewed by the project lead (whoever is responsible for the
capsmodel-owner@carcc.org role). 
* Code must be reasonable commented and structured. 
* All code is subject to the ECL 2.0 license, and must include the copyright/license statement included below.

# Governing license
>	Copyright 2021 CaRCC. Licensed under the
>	Educational Community License, Version 2.0 (the "License"); you may
>	not use this file except in compliance with the License. You may
>	obtain a copy of the License at
>	
>	[http://www.osedu.org/licenses/ECL-2.0]
>
>	Unless required by applicable law or agreed to in writing,
>	software distributed under the License is distributed on an "AS IS"
>	BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
>	or implied. See the License for the specific language governing
>	permissions and limitations under the License.