function onEdit(e) {
  var sheet = e.range.getSheet();
  var spreadsheet = e.source;

  // get the first row with the column headings  
  var headersRange = sheet.getRange(1, 1, 1, 20);

  // Creates  a text finder for the range.
  var textFinder = headersRange.createTextFinder('Deployment at Institution');
  var diffusionColRange = textFinder.findNext();
  var diffusionCol = (diffusionColRange != null) ? diffusionColRange.getColumn() : 5; // Default to best guess

  // For the Facings sheets, if they select "No Deployment or Support" in the Diffusion column, default the other two columns accordingly
  if (sheet.getName().search("Facing" >=0) 
      && e.range.getColumn() == diffusionCol 
      && e.value != null 
      && e.value.search("No deployment") >=0 ) {
    var row = e.range.getRow();
    // Set the value in the Cross-Institutional Diffusion column to the corresponding "No collaboration" value as well.
    var namedRange = spreadsheet.getRangeByName('MultiInstCollabValues');
    if(namedRange != null) {
      var cellToUpdate = sheet.getRange(row,diffusionCol+1);
      var noSupportValue = namedRange.getValue();
      cellToUpdate.setValue(noSupportValue);
    }
    // Set the value in the Service Operating Level column to the corresponding "No support" value as well.
    namedRange = spreadsheet.getRangeByName('SOLValues');
    if(namedRange != null) {
      var cellToUpdate = sheet.getRange(row,diffusionCol+2);
      var noSupportValue = namedRange.getValue();
      cellToUpdate.setValue(noSupportValue);
    }
  }
}
